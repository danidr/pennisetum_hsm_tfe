## Data filtering: P. setaceum's occurence in Tenerife island 2005-2014
## Daniele Da Re, Enrico Tordoni
## 28.05.2020

#+--------------setwd and load pkgs 
setwd("/F:/UCLouvain/Backup20191112/working_files/Pennisetum/data_occurences")
library(raster)
library(foreign)
library(maptools)
library(PBSmapping)
library(rgeos)
library(spThin)
library(gridExtra)
library(ggmap)
library(xtable)

#+-----load ATLANTIS database and select occurrence from 2005 to 2014-----
list.files()
utm28N<-" +proj=utm +zone=28 +datum=WGS84 +units=m +no_defs +ellps=WGS84 +towgs84=0,0,0 "
pen2019<-shapefile("pennis_ATLANTISgrid500_2019/pennis.shp")
pen2019_info<-read.dbf("pennis_ATLANTISgrid500_2019/pennis_documentos.dbf")
pen2019_info_from2005<-pen2019_info[which(pen2019_info$AnioCita >= 2005 & pen2019_info$AnioCita <= 2014), ]
# pen2019_info_from2005_ID<-data.frame("ID"=unique(pen2019_info_from2005$IdCeldaTax))
pen2019_info_from2005_ID<-data.frame("ID"=pen2019_info_from2005$IdCeldaTax)
head(pen2019_info_from2005_ID)
vect<-pen2019_info_from2005_ID$ID

#+----calculate the centroids of all the grid cell
pen2019_coord<-data.frame(coordinates(pen2019))
coordinates(pen2019_coord)<-c("X1", "X2")
plot(pen2019)
plot(pen2019_coord, col='blue', add=TRUE)
pen2019_coord$id<-seq(1:nrow(pen2019_coord@coords))
pen2019_pts<-cbind(pen2019_coord, pen2019, pen2019_coord@coords )
summary(pen2019_pts@data)

#+----select the centroid of the grid cell sampled between 2005 and 2014
pen2019_pts_selected<-na.omit(pen2019_pts@data[vect, ])
coordinates(pen2019_pts_selected)<-c("X1", "X2")
pen2019_pts_selected
crs(pen2019_pts_selected)<-utm28N
plot(pen2019_pts)
plot(pen2019_pts_selected, col='green', add=TRUE)
# shapefile(pen2019_pts_selected, "pen2019_pts_selected.shp", overwrite=T)

#+----Update ATLANTIS database with Teno's Rural Park sampling data ----- 
# pen_tot_2016<-shapefile("pennisetum_total_point.shp")
pen_teno<-shapefile("Pennisetum setaceum 2012-2015 Parque de Teno/Pennisetum Teno 2012-2015.shp")
head(pen_teno)
pen_teno<-pen_teno[which(pen_teno$Afect2014>0),c(1:2, 5:6)]
plot(pen_teno)

#load 500m point grid covering the whole island 
grid_pt<-read.csv("/home/ddare/OneDrive/BGLSM_review/BGLSM_October_2018/BGLSM_2018/centroidi_qgis_05febbraio.csv")
grid_pt<-cbind(grid_pt, "X1"=grid_pt$X, "X2"=grid_pt$Y)
coordinates(grid_pt)<-c("X", "Y")
crs(grid_pt)<-utm28N

#select grid cell intersecting Teno's sampling plot 
out <- over(pen_teno,grid_pt )
out<-na.omit(out)
out<-out[,c(1, 9:10)]
coordinates(out)<-c("X1", "X2")
crs(out)<-utm28N
out$id<-seq(1:nrow(out@coords))
plot(out)

plot(pen2019_pts_selected)
plot(out, col='blue', add=TRUE)

pen2019_ATL_Teno <-data.frame(rbind(pen2019_pts_selected@coords, out@coords))

coordinates(pen2019_ATL_Teno)<-c("X1", "X2")
crs(pen2019_ATL_Teno)<-utm28N
plot(pen2019_ATL_Teno)
pen2019_ATL_Teno$id<-seq(1:nrow(pen2019_ATL_Teno@coords))
pen2019_ATL_Teno$X<-pen2019_ATL_Teno@coords[1]
pen2019_ATL_Teno$Y<-pen2019_ATL_Teno@coords[2]
pen2019_ATL_Teno$Species<-rep("P.setaceum",nrow(pen2019_ATL_Teno@coords))

# atl_name<-data.frame()
atl_name<-rep("ATLANTIS", nrow(pen2019_pts_selected))
teno_name<-rep("Teno Rural Park", nrow(out))
dbName<-c(atl_name,teno_name)
pen2019_ATL_Teno$dbName<-dbName

# shapefile(pen2019_ATL_Teno, "pen_full_2019.shp", overwrite=T)
head(pen2019_ATL_Teno)
# occ_tab<-pen2019_ATL_Teno@data
write.csv(pen2019_ATL_Teno@data, "F:/UCLouvain/Backup20191112/working_files/Pennisetum/maxent_tenerife/03_Analisi_TFE/2019_plots/unfiltered_occurences_tab.csv" )
# print(xtable(pen2019_ATL_Teno@data, type = "latex"), file = "/home/ddare/OneDrive/Pennisetum/maxent_tenerife/03_Analisi_TFE/2019_plots/unfiltered_occurences_tab.tex")


#+----spatial thinning -----
pen2019_ATL_Teno
wgs84<-"+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs"
pen2019_ATL_Teno_wgs84<-spTransform(pen2019_ATL_Teno,wgs84)

pen_wgs84<-pen2019_ATL_Teno_wgs84@coords
pen_wgs84<-data.frame(pen_wgs84)
names(pen_wgs84)<-c("Long", "Lat")
pen_wgs84$species<-rep("spec",nrow(pen_wgs84))

pen_thin<-thin(loc.data= pen_wgs84, 
               lat.col = "Lat",  long.col = "Long", 
               spec.col = "species", 
               thin.par = 2, # km
               reps = 100, 
               locs.thinned.list.return = TRUE, 
               write.file=T,
               max.files=3,
               out.dir= "penn_2019_tinned/",
               out.base ="pen_thin", 
               write.log.file = T,
               log.file = "penn_2019_tinned/pen_thin_log_file.txt")

summaryThin(pen_thin)
pen_thinned<-read.csv("F:/UCLouvain/Backup20191112/working_files/Pennisetum/data_occurences/penn_2019_tinned/pen_thin_thin2_new.csv")
head(pen_thinned)
coordinates(pen_thinned)<-~Long + Lat
crs(pen_thinned)<-wgs84
pen_thinned28N<-spTransform(pen_thinned,utm28N)

plot(pen2019_pts_selected)
plot(pen_thinned28N, col='red', add=TRUE)

#+----occurences maps ----
#TFE map
penWGS84_df<-data.frame(coordinates(pen_thinned))

bbox<-data.frame("Longitude"=c(-17, -16), "Latitude"=c(27.9,28.7))
sbbox <- make_bbox(lon = bbox$Longitude, lat = bbox$Latitude, f = .1)
sbbox
bc_big <- get_map(location = sbbox,  source = "stamen", maptype = "terrain", zoom=10)
m2<-ggmap(bc_big) + 
  geom_point(data=penWGS84_df, aes(x=Long, y=Lat),size=1.5, colour= "red", alpha = 0.5) +
  xlab("Longitude") +  ylab("Latitude")+
  theme(text = element_text(size=14))

m2

outname="F:/UCLouvain/Backup20191112/working_files/Pennisetum/maxent_tenerife/03_Analisi_TFE/2019_plots/20200528/occurence_filtered_20190528.pdf"
pdf(file=outname, height=8, width=12)
m2
dev.off()

#+----check collineary of predictors ----
#as predictors: Mean winter precipitation + mean spring precipitation, Mean spring temperatures, road density
rlist<-list.files("F:/UCLouvain/Backup20191112/working_files/Pennisetum/maxent_tenerife/centraline", pattern= ".tif$", full.names = T)
rast_stack<-stack(rlist)
rast_stack<-scale(rast_stack)
jnk=layerStats(rast_stack, 'pearson', na.rm=T)
jnk=layerStats(rast_stack[[c(3:4,10)]], 'pearson', na.rm=T)
corr_matrix=jnk$'pearson correlation coefficient'
corr_matrix

print(xtable(corr_matrix, type = "latex"), file = "/home/ddare/OneDrive/Pennisetum/maxent_tenerife/03_Analisi_TFE/2019_plots/pred_corr_matrix.tex")

df_extract<-extract(rast_stack, pen_thinned28N)
cor(df_extract, use="complete.obs")
cor(df_extract[,c(3:4,10)], use="complete.obs")

#+----Plot predictors  ----
envs<-rast_stack[[c(3:4,10)]]
names(envs)
them<-rev(brewer.pal(8,"BrBG"))
mapTheme <- rasterTheme(region=them)

s3<-levelplot(envs[[1]],  scales=list(draw=FALSE), contour = FALSE, margin = FALSE,  par.settings = BuRdTheme)#, main= "Precipitations ")
s5<-levelplot(envs[[2]],  scales=list(draw=FALSE), contour = FALSE, margin = FALSE,  par.settings = YlOrRdTheme)#, main= "Road Kernel Density")
s6<-levelplot(envs[[3]],  scales=list(draw=FALSE), contour = FALSE, margin = FALSE,  par.settings = BuRdTheme)#, main= "Temperatures")

pp1<-grid.arrange(  s3,s6, s5, nrow=1)

outname="F:/UCLouvain/Backup20191112/working_files/Pennisetum/maxent_tenerife/03_Analisi_TFE/2019_plots/20200528/Predictors_20190528.pdf"
pdf(file=outname, height=8, width=12)
cowplot::ggdraw(pp1) + 
  theme(plot.background = element_rect(fill="white", color = NA))
dev.off()

#occurences cumulative density distribution 
df_extract=as.data.frame(df_extract)
outname="F:/UCLouvain/Backup20191112/working_files/Pennisetum/maxent_tenerife/03_Analisi_TFE/2019_plots/20200528/Presence_cumulDistr_20190528.pdf"
pdf(file=outname, height=8, width=12)
plot(ecdf(df_extract$dem_ascii), cex=0.5,col="black", main="Presence cumulative density distribution ", xlab="Elevation", ylab="Fn(Elevation)")
dev.off()





